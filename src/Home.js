import { useState, useEffect } from 'react';
const Home = () => {
  const [data, setData] = useState('');
  useEffect(async () => {
    fetch('http://localhost:5555/api/data').then(res => res.json()).then(data => {
      console.log('data = ', data);
      setData(data.data);
    });
  }, []);

  return (
    <div>
      <div>Home page</div>
      <div>message from api: {data}</div>
    </div>
  );
};

export default Home;
