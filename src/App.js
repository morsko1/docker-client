import './App.css';
import Home from './Home';
import About from './About';
import NotFound from './NotFound';
import { Route, Switch, Link } from 'react-router-dom';

function App() {
  return (
    <div className='App'>
      test react app!
      <br />
      <Link to='/'>Home</Link>
      <br />
      <Link to='/about'>About</Link>
      <Switch>
        <Route exact path='/' component={Home}/>
        <Route exact path='/about' component={About}/>
        <Route component={NotFound}/>
      </Switch>
    </div>
  );
}

export default App;
